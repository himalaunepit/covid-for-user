import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
export default class EditProfile extends React.Component {
    constructor(){
        super();
    }
  render(){
    return (
      <View style={styles.container}>
        <View style={styles.textinput}>
        <TextInput  
                style={styles.inputText}
                placeholder=" Full Name" 
                placeholderTextColor="#A9A8A6"
                onChangeText={text => this.setState({fullname:text})}/>
        </View>

        <View style={styles.textinput}>
        <TextInput  
                style={styles.inputText}
                placeholder=" Address" 
                placeholderTextColor="#A9A8A6"
                onChangeText={text => this.setState({fullname:text})}/>
        </View>

        <View style={styles.textinput}>
        <TextInput  
                style={styles.inputText}
                placeholder=" Tagline " 
                placeholderTextColor="#A9A8A6"
                onChangeText={text => this.setState({fullname:text})}/>
        </View>

        
        

        <View style={styles.btnstyle}>
        <TouchableHighlight style={styles.loginBtn}>
                   <Text style={styles.loginText}>UPDATE</Text>
                </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        height:hp('100%'),
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white'
    },
   
    textinput:{
        // flex:0.5,
        height:hp('10%'),
        width:wp('85%'),
        justifyContent:'center',
    },
    inputText:{
        height:hp('6%'),
        color:"#808080",
        borderColor: 'gray', 
        borderWidth: hp('0.1%'), 
        borderRadius:wp('1%'),
        fontSize:wp('4%'),
      },
   
  
      btnstyle:{
        // flex:3,
        height:hp('55%'),
        width:wp("85%"),
        justifyContent:'flex-end',
        alignItems:'center',
        marginBottom:hp('12%')
      },
      loginBtn:{
        backgroundColor:'#15B2A2', 
        paddingTop:hp("1.7%"), 
        paddingLeft:wp('2.7%'), 
        paddingRight:wp('2.7%'),
        paddingBottom:hp('1.7%'),
        width:"100%",
        borderRadius:wp('3.5%')
      },
      loginText:{
        color:'white', 
        fontWeight:'bold',
        textAlign:'center',
        fontSize:wp('2.5%')
      },

})