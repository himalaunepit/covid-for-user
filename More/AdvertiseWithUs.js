import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
export default class AdvertiseWithUs extends React.Component {
    constructor(){
        super();
    }
  render(){
    return (
      <View style={styles.container}>
        <View style={styles.mainContainer}> 
        <View style={styles.textinput}>
        <TextInput  
                style={styles.inputText}
                placeholder=" Full Name" 
                placeholderTextColor="#A9A8A6"
                onChangeText={text => this.setState({fullname:text})}/>
        </View>

        <View style={styles.textinput}>
        <TextInput  
                style={styles.inputText}
                placeholder=" Business Name" 
                placeholderTextColor="#A9A8A6"
                onChangeText={text => this.setState({fullname:text})}/>
        </View>

        <View style={styles.textinput}>
        <TextInput  
                style={styles.inputText}
                placeholder=" Email " 
                placeholderTextColor="#A9A8A6"
                onChangeText={text => this.setState({fullname:text})}/>
        </View>

        <View style={styles.textinput}>
        <TextInput  
                style={styles.inputText}
                placeholder=" Subject  " 
                placeholderTextColor="#A9A8A6"
                onChangeText={text => this.setState({fullname:text})}/>
        </View>

        <View style={styles.textareastyle}>
        <TextInput
      style={styles.textArea}
      underlineColorAndroid="transparent"
      placeholder="  Message"
      placeholderTextColor="grey"
      numberOfLines={4}
      multiline={true}
    />
        </View>

        <View style={styles.btnstyle}>
        <TouchableHighlight style={styles.loginBtn}>
                   <Text style={styles.loginText}>SUBMIT</Text>
                </TouchableHighlight>
        </View>

        </View>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        // flex:1,
        height:hp('100%'),
        flexDirection:'column',
        justifyContent:'flex-start',
        alignItems:'center',
        backgroundColor:'white',
    },
    mainContainer:{
       marginTop:hp('3%')
    },
   
    textinput:{
        // flex:0.7,
        height:hp('8%'),
        width:wp('90%'),
        justifyContent:'center',
    },
    inputText:{
        height:hp('5%'),
        color:"#808080",
        borderColor: 'gray', 
        borderWidth: hp('0.1%'), 
        borderRadius:wp('1%'),
        fontSize:wp('4%'),
      },
    textareastyle:{
        // flex:2,
        height:hp('15%'),
        width:wp('90%'),
        borderColor:'gray',
        borderWidth:hp('0.1%'),
        borderRadius:wp('1%'),
        marginTop:hp('2%'),
    },
    textArea: {
        justifyContent: "flex-start",
        fontSize:wp('4%'),

      },
      btnstyle:{
        // flex:1,
        height:hp('33%'),
        width:wp("90%"),
        justifyContent:'flex-end',
        alignItems:'center',
      },
      loginBtn:{
        backgroundColor:'#15B2A2', 
        paddingTop:hp("1.7%"), 
        paddingLeft:wp('2.7%'), 
        paddingRight:wp('2.7%'),
        paddingBottom:hp('1.7%'),
        width:"100%",
        borderRadius:wp('3.5%')
      },
      loginText:{
        color:'white', 
        fontWeight:'bold',
        textAlign:'center',
        fontSize:wp('2.5%')
      },

})