import React, { useState } from 'react'
import {View,Text,StyleSheet,TextInput,TouchableHighlight} from 'react-native'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
function LoginScreen(props){
  const [email,setEmail] =useState('')
  const [password,setPassword] = useState('')
  return(
      <View style={styles.mainContainer}>
          <View style={styles.mainHeader}>
            <Text style={styles.firstText}>Welcome</Text>
            <Text style={styles.secondText}>back !</Text>
          </View>

          <View style={styles.form}>
             <TextInput  
              style={styles.inputText}
              placeholder="  Username" 
              placeholderTextColor="#A9A8A6"
              onChangeText={text => setEmail({email:text})}/>
              <TextInput  
              secureTextEntry
              style={styles.inputText}
              placeholder=" Password" 
              placeholderTextColor="#A9A8A6"
              onChangeText={text =>setPassword({password:text})}/>
          </View>

          <View style={styles.textContent}>
             <Text style={styles.termsText}>By Registering or Signing In,You Agree our Terms and</Text>
             <Text style={styles.termsText}>conditions</Text>
          </View>

          <View style={styles.btn}>
                            <TouchableHighlight style={styles.loginBtn} onPress={()=>props.navigation.navigate("Home")}>
                    <Text style={styles.loginText}>SUBMIT</Text>
             </TouchableHighlight>
          </View>

          <View style={styles.forgotPassword}>
           <Text style={styles.forgotText} onPress={()=>props.navigation.navigate('ForgotPassword')}>Forgot Your Password?</Text>
          </View>
      </View>
  )
}
export default LoginScreen

const styles = StyleSheet.create({
  mainContainer:{
        backgroundColor:'white',
        justifyContent:'flex-start',
        alignItems:'center',
        width:wp('100%'),
        height:hp('100%'),
  } ,
  mainHeader:{
    flexDirection:'row',
    width:wp('82%'),
    justifyContent:'space-between',
    alignItems:'flex-end',
    height:hp('10%'),
  },
  firstText:{
     fontSize:hp('5%'),
     fontWeight:'bold'
  },
  secondText:{
    fontSize:hp('7%'),
    fontWeight:'bold',
    color:'#C1C0C9'
  },
  form:{
    height:hp('30%'),
    justifyContent:'flex-end',
    width:wp('80%')
  },
    inputText:{
    height:hp('6%'),
    color:"#C1C0C9",
    borderColor: '#C1C0C9', 
    borderWidth: wp('0.3%'), 
    borderRadius:wp('1%'),
    fontSize:wp('4%'),
    marginVertical:hp('1%')
  },
  textContent:{
    width:wp('73%'),
    height:hp('15%'),
    justifyContent:'flex-end',
    textAlign:'center',
    alignItems:'center',
  },
  btn:{
    height:hp('30%'),
    justifyContent:'flex-end',
    width:wp('80%')
  },
  loginBtn:{
    backgroundColor:'#696969', 
    paddingTop:hp("1.7%"), 
    paddingLeft:wp('2.7%'), 
    paddingRight:wp('2.7%'),
    paddingBottom:hp('1.7%'),
    width:"100%",
    borderRadius:wp('3.5%')
  },
  loginText:{
    color:'white', 
    fontWeight:'bold',
    textAlign:'center',
    fontSize:wp('2.5%')
  },
  forgotPassword:{
    alignItems:'center',
    height:hp('9%'),
  },
  forgotText:{
     color:'#C0C1C9',
     margin:hp('2.5%')
  },
  termsText:
  {
    color:"#C1C0C9"
  }
})
