import React, { useState } from 'react'
import {View,Text,StyleSheet,TextInput,TouchableHighlight} from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
function EnterCode(props){
  const [email,setEmail] = useState('')
  return(
      <View style={styles.mainContainer}>
          <View style={styles.mainHeader}>
            <Text style={styles.firstText}>Enter</Text>
            <Text style={styles.secondText}>Code </Text>
          </View>

  
          <View style={styles.form}>
             <TextInput  
              style={styles.inputText}
              placeholder="  Enter Code here" 
              placeholderTextColor="#A9A8A6"
              onChangeText={text =>setEmail({email:text})}/>
          </View>

    

          
          <View style={styles.btn}>
                            <TouchableHighlight style={styles.loginBtn} onPress={()=>props.navigation.navigate("NewPassword")}>
                    <Text style={styles.loginText}>SUBMIT</Text>
             </TouchableHighlight>
          </View>
      </View>
  )
}
export default EnterCode

const styles = StyleSheet.create({
  mainContainer:{
        backgroundColor:'white',
        justifyContent:'flex-start',
        alignItems:'center',
        width:wp('100%'),
        height:hp('100%'),
  } ,
  mainHeader:{
    flexDirection:'row',
    width:wp('82%'),
    alignItems:'flex-end',
    height:hp('7%'),
  },
  firstText:{
     fontSize:hp('4%'),
     fontWeight:'bold'
  },
  secondText:{
    fontSize:hp('5%'),
    fontWeight:'bold',
    color:'#C1C0C9',
    marginLeft:wp('2%')
  },
  form:{
    height:hp('15%'),
    justifyContent:'flex-end',
    width:wp('80%'),
  },
    inputText:{
    height:hp('6%'),
    color:"#C1C0C9",
    borderColor: '#C1C0C9', 
    borderWidth: wp('0.3%'), 
    borderRadius:wp('1%'),
    fontSize:wp('4%'),
    marginVertical:hp('1%'),
  },
 
  btn:{
    height:hp('63%'),
    justifyContent:'flex-end',
    width:wp('80%')
  },
  loginBtn:{
    backgroundColor:'#696969', 
    paddingTop:hp("1.7%"), 
    paddingLeft:wp('2.7%'), 
    paddingRight:wp('2.7%'),
    paddingBottom:hp('1.7%'),
    width:"100%",
    borderRadius:wp('3.5%')
  },
  loginText:{
    color:'white', 
    fontWeight:'bold',
    textAlign:'center',
    fontSize:wp('2.5%')
  },
})
