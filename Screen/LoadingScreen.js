import  React,{useEffect} from 'react';
import { Text, View,Button,Image } from 'react-native';
function LoadingScreen({navigation}) {
    useEffect(() => {
        setTimeout(() => {
            navigation.navigate("Onboarding")
        }, 2000);
      }, []);
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',backgroundColor:'white' }}>
        <Image  source={require('../assets/logoimage.png')} style={{height:120,width:200,alignItems:'center',justifyContent:"center"}}/>
      </View>
    );
  }
  export default LoadingScreen