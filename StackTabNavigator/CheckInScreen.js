import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight,Image } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
export default class CheckInScreen extends React.Component {

  render(){
    return (
      <View style={styles.container}>
          <View style={styles.headerStyle}>
                    <Text style={{fontWeight:'bold',fontSize:wp('4%'),marginBottom:hp('1%')}}>Scan QR</Text>
        </View>
        <View style={styles.textforinfo}>
           <Text style={{fontSize:wp('4%'),fontWeight:'bold'}}>Point Your camera at QR code to Scan</Text>
        </View>
        <View style={styles.scancamera}>
        <Image style={styles.avatar} source={require('../assets/scanqr.png')}/>
        </View>
        <View style={styles.btnstyle}>
        <TouchableHighlight style={styles.loginBtn}>
                   <Text style={styles.loginText}>CLOSE</Text>
                </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white'
    },
  
    textforinfo:{
        flex:1,
        width:wp('80%'),
        justifyContent:'center',
        alignItems:'center'
    },
    scancamera:{
      flex:4,
      justifyContent:'center',
      alignItems:'center',
    },
   avatar:{
        height:270,
        width:270,
   },
    btnstyle:{
      flex:1,
      width:wp("80%"),
      justifyContent:'center'
  },
  loginBtn:{
      backgroundColor:'#696969', 
      paddingTop:hp("1.5%"), 
      paddingLeft:wp('2.5%'), 
      paddingRight:wp('2.5%'),
      paddingBottom:hp('1.5%'),
      width:"100%",
      borderRadius:wp('3.5%')
    },
    loginText:{
      color:'white', 
      fontWeight:'bold',
      textAlign:'center',
      fontSize:wp('2.5%')
    },
    headerStyle:{
      height:hp('8%'),
      justifyContent:'flex-end',
      alignItems:'center',
      width:wp('90%'),
      marginTop:hp('1%'),
      borderBottomColor:'#e6e6e6',
      borderBottomWidth:wp('0.4%')
    
    },

})