import * as React from 'react';
import { View, Button,Text,StyleSheet } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
function NotificationScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start',backgroundColor:'white' }}>
      <View style={styles.headerStyle}>
                    <Text style={{fontWeight:'bold',fontSize:wp('4%'),marginBottom:hp('1%')}}>Notification (2)</Text>
        </View>
      <View style={{backgroundColor:'#C1C0C9',marginTop:hp('2%')}}>
      <View style={styles.maincontainer}>
          <View style={styles.iconStyle}>
          <MaterialCommunityIcons name="source-fork" size={20}/>
          </View>
          <View style={styles.textContent}>
            <Text style={styles.headertext}>Low Risk of Infections</Text>
            <Text>Using gels we can treatment,Using gels we can treatment using Using gels we can treatment...</Text>
          </View>
      </View>
      </View>

      <View style={{backgroundColor:'#C1C0C9',marginTop:hp('2%')}}>
      <View style={styles.maincontainer}>
          <View style={styles.iconStyle}>
          <MaterialCommunityIcons name="source-fork" size={20}/>
          </View>
          <View style={styles.textContent}>
            <Text style={styles.headertext}>Health Tips for Today</Text>
            <Text>Using gels we can treatment,Using gels we can treatment using Using gels we can treatment...</Text>
          </View>
      </View>
      </View>


      <View style={{backgroundColor:'#C1C0C9',marginTop:hp('2%')}}>
      <View style={styles.maincontainer}>
          <View style={styles.iconStyle}>
          <MaterialCommunityIcons name="source-fork" size={20}/>
          </View>
          <View style={styles.textContent}>
            <Text style={styles.headertext}>What to do ?</Text>
            <Text>Using gels we can treatment,Using gels we can treatment using Using gels we can treatment...</Text>
          </View>
      </View>
      </View>

      

    </View>
  );
}
export default NotificationScreen
const styles = StyleSheet.create({
  maincontainer:{
          flexDirection:'row',
          justifyContent:'center',
          alignItems:'center',
          width:wp('80%'),
          margin:hp('3%'),
  },
  iconStyle:{
      justifyContent:'center',
      alignItems:'center',
      height:40,
      width:40,
      borderRadius:20,
      backgroundColor:'#ADD8E6'
  },

  textContent:{
    marginLeft:wp('2%'),
  },
  headerStyle:{
    height:hp('9%'),
    justifyContent:'flex-end',
    alignItems:'center',
    width:wp('90%'),
    marginTop:hp('1%'),
    borderBottomColor:'#e6e6e6',
    borderBottomWidth:wp('0.4%')
  
  },
  headertext:{
    fontSize:wp('3%'),
    fontWeight:'bold'
  }
})
