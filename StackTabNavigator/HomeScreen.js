import * as React from 'react';
import { View, Button,Text,Image,StyleSheet, ImageBackground } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { setStatusBarBackgroundColor } from 'expo-status-bar';
function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start',backgroundColor:'white' }}>
          <View style={styles.headerStyle}>
                    <Text style={{fontWeight:'bold',fontSize:wp('5%')}}></Text>
        </View>
        <View style={{flexDirection:'row',justifyContent:'flex-end',width:wp('90%'),marginTop:hp('3%')}}>
                <Text style={{color:'gray',fontSize:wp('4%'),marginTop:hp('1%')}}>Ads</Text>
                 <View style={{height:26,width:26,borderRadius:13,backgroundColor:'#e6e6fa',justifyContent:'center',alignItems:'center',marginTop:wp('1%')}}>
                  <MaterialCommunityIcons name='close' size={12} color={'black'}/>
                </View>
           </View>
          <ImageBackground source={require('../assets/restraurant.png')} style={{height:hp('18%'),backgroundColor:'gray',width:wp('90%')}}>
              <View style={{justifyContent:'center',alignItems:'center'}}>
                <View style={{height:hp('7%'),padding:wp('2%'),borderWidth:wp('0.4%'),borderColor:'white',justifyContent:'center',alignItems:'center',marginTop:hp('2%')}}>
                <Text style={{fontSize:wp('4%'),fontWeight:'bold',color:'white'}}>Give Us Check In And</Text>
                <Text  style={{fontSize:wp('4%'),fontWeight:'bold',color:'white'}}>Get 5% Discount</Text>
                </View>
              </View>
              <View style={{width:wp('40%'),height:hp('7%'),alignItems:'flex-start',marginLeft:wp('29%'),marginTop:hp('2%')}}> 
              <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <MaterialCommunityIcons name='arrow-right-drop-circle' color={'white'}/>
                <Text style={{marginLeft:wp('1.5%'),fontWeight:'bold',color:'white'}}>Use free WIFI</Text>
              </View>
              <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <MaterialCommunityIcons name='arrow-right-drop-circle' color={'white'}/>
                <Text style={{marginLeft:wp('1.5%'),fontWeight:'bold',color:'white'}}>Discount For Couple</Text>
              </View>
              <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <MaterialCommunityIcons name='arrow-right-drop-circle' color={'white'}/>
                <Text style={{marginLeft:wp('1.5%'),fontWeight:'bold',color:'white'}}>Birthday Discount</Text>
              </View>
              </View>
          </ImageBackground>
        <View style={{flexDirection:'row',justifyContent:'space-between',width:wp('90%')}}>
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                    <MaterialCommunityIcons name='map-marker'/>
                    <Text style={{marginLeft:wp('1.5%'),fontWeight:'bold'}}>S-14 Sekhar</Text>
                </View>
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',borderLeftWidth:2,borderLeftColor:'gray'}}>
                    <MaterialCommunityIcons name='phone-outline'/>
                     <Text style={{marginLeft:wp('1.5%'),fontWeight:'bold'}}>9860552717</Text>
                </View>
                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',borderLeftWidth:2,borderLeftColor:'gray'}}> 
                <View style={{marginRight:wp('1%'),height:20,width:20,borderRadius:10,backgroundColor:'#00ced1',justifyContent:'center',alignItems:'center'}}>
                  <MaterialCommunityIcons name='twitter'color={'white'}/>
                </View>
               <View style={{marginRight:wp('1%'),height:20,width:20,borderRadius:10,backgroundColor:'#0000ff',justifyContent:'center',alignItems:'center'}}>
               <MaterialCommunityIcons name='facebook'color={'white'} />
               </View>
                <View style={{height:20,width:20,borderRadius:10,backgroundColor:'#6495ed',justifyContent:'center',alignItems:'center'}}>
                <MaterialCommunityIcons name='instagram' color={'white'}/>
                </View>
                </View>
           </View>
        <View style={{    borderBottomColor:'#e6e6e6',
    borderBottomWidth:wp('0.4%'),width:250}}>
           <Image source={require('../assets/qr.png')} style={styles.qrImage} />
        </View>

        <View style={styles.recentActivity}>
          <Text style={{fontSize:wp('5%'),fontWeight:'bold',marginLeft:wp('2%')}}>Recent Activity</Text>
           <MaterialCommunityIcons name="chevron-right" size={wp('8%')} onPress={()=>navigation.navigate('RecentActivity')}/>
        </View>
    </View>
  );
}
export default HomeScreen

const styles = StyleSheet.create({
  qrImage:{
       height:250,
       width:250,
       marginTop:hp('5%')
  } ,
  recentActivity:{
    marginTop:hp('3%'),
    height:hp('8%'),
    width:wp('90%'),
    backgroundColor:'white',
    flexDirection:"row",
    justifyContent:'space-between',
    alignItems:'center',
    borderRadius:hp('2.5%'),
    borderWidth:hp('0.2%'),
    borderColor:'#e6e6e6',
  },
  headerStyle:{
    height:hp('8%'),
    justifyContent:'center',
    alignItems:'center',
    width:wp('90%'),
    marginTop:hp('1%'),
    borderBottomColor:'#e6e6e6',
    borderBottomWidth:wp('0.4%')
  
  },
 
})