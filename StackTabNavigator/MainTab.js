import React,{useEffect} from 'react'
import { View, Button,Image,Text,StyleSheet } from 'react-native';
import {
  NavigationContainer,
  getFocusedRouteNameFromRoute,
} from '@react-navigation/native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { MaterialCommunityIcons } from '@expo/vector-icons';
//Buttom Tab Container
import HomeScreen from './HomeScreen'
import NotificationScreen from './NotificationScreen'
import CheckInScreen from './CheckInScreen'
import ContactsScreen from './ContactsScreen'
import MoreScreen from './MoreScreen'
import AdvertiseWithUs from '../More/AdvertiseWithUs'
import Support from '../More/Support'
import PrivacyPolicy from '../More/PrivacyPolicy'
import NotificationSetting from '../More/NotificationSetting'
import TermsOfUse from '../More/TermsOfUse'
import EditProfile from '../More/EditProfile'
import RecentActivity from '../More/RecentActivity'

//Screen
import LoadingScreen from '../Screen/LoadingScreen'
import Onboarding from '../OnboardingScreen/Intro'
import OnboardingScreen from '../Screen/OnboardingScreen'
import RegisterScreen from '../Screen/RegisterScreen'
import LoginScreen from '../Screens/LoginScreen'
import ForgotPassword from '../Screen/ForgotPassword'
import EnterCode from '../Screen/EnterCode'
import NewPassword from '../Screen/NewPassword'



function getHeaderTitle(route) {
  // If the focused route is not found, we need to assume it's the initial screen
  // This can happen during if there hasn't been any navigation inside the screen
  // In our case, it's "Feed" as that's the first screen inside the navigator
  const routeName = getFocusedRouteNameFromRoute(route) ?? 'HomeScreen';

  switch (routeName) {
    case 'HomeScreen':
      return 'Home';
    case 'NotificationScreen':
      return 'Notification (2)';
    case 'CheckInScreen':
      return 'Scan QR';
      case 'ContactsScreen':
      return 'Profile';
    case 'MoreScreen':
      return 'Settings';
  }
}

const Tab = createBottomTabNavigator();

function HomeTabs({ navigation, route }) {
  React.useLayoutEffect(() => {
    navigation.setOptions({ headerTitle: getHeaderTitle(route) });
  }, [navigation, route]);

  return (
    <Tab.Navigator   tabBarOptions={{
      activeTintColor:'#15B2A2',
      style:{height:hp('7%'),backgroundColor:'#D3D3D3'}
    }}>
      <Tab.Screen name="HomeScreen" component={HomeScreen}  options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={hp('3.5%')} />
          ),
        }}/>
      <Tab.Screen name="NotificationScreen" component={NotificationScreen} 
      options={{
        tabBarLabel: 'Cart',
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="bell" color={color} size={30} />
        ),
      }}/>
      <Tab.Screen name="CheckInScreen" component={CheckInScreen} 
      options={{
        tabBarLabel: 'List Product',
        tabBarIcon: ({ color, size }) => (
          <View style={styles.circleIcon}>
            <MaterialCommunityIcons name="qrcode" color={'white'} size={30} />
          </View>
       
        ),
      }}/>
      <Tab.Screen name="ContactsScreen" component={ContactsScreen} 
      options={{
        tabBarLabel: 'Profile',
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="account" color={color} size={30} />
        ),
      }}/>
      <Tab.Screen name="MoreScreen" component={MoreScreen} 
      options={{
        tabBarLabel: 'Setting',
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="settings" color={color} size={30} />
        ),
      }}/>
    </Tab.Navigator>
  );
}

const Stack = createStackNavigator();

export default function MainTab() {

  return (
    <NavigationContainer>
      <Stack.Navigator  screenOptions={{headerTitleAlign: 'center',headerBackImage: () =>  <MaterialCommunityIcons name="chevron-left" size={hp('4.5%')} color={'#262628'} style={{marginLeft:wp('3.5%')}}/>,
    headerStyle: { backgroundColor: '#fff', elevation:0 }
  }}>
      <Stack.Screen name="LoadingScreen" component={LoadingScreen}  options={{headerShown: false}} />
      <Stack.Screen name="Onboarding" component={Onboarding}  options={{headerShown: false}} />
      <Stack.Screen name="RegisterScreen" component={RegisterScreen} options={{headerTitle: false}}/>  
      <Stack.Screen name="LoginScreen" component={LoginScreen} options={{headerTitle: false}} /> 
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} options={{headerTitle: false}}/> 
        <Stack.Screen name="EnterCode" component={EnterCode} options={{headerTitle: false}}/>    
        <Stack.Screen name="NewPassword" component={NewPassword} options={{headerTitle: false}}/> 
        <Stack.Screen name="Home" component={HomeTabs} options= {{headerShown:null}}/>
        <Stack.Screen name="AdvertiseWithUs" component={AdvertiseWithUs} options={{title:"Advertise With Us",headerStyle:{borderBottomColor:'#e6e6e6',borderBottomWidth:wp('0.4%')}  }} />
        <Stack.Screen name="Support" component={Support} options={{title:"Support",headerStyle:{borderBottomColor:'#e6e6e6',borderBottomWidth:wp('0.4%')}  }}/>
        <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy} options={{title:"Privacy Policy",headerStyle:{borderBottomColor:'#e6e6e6',borderBottomWidth:wp('0.4%')}}} />
        <Stack.Screen name="NotificationSetting" component={NotificationSetting}  options={{title:"Notification Setting",headerStyle:{borderBottomColor:'#e6e6e6',borderBottomWidth:wp('0.4%')}}}/>
        <Stack.Screen name="TermsOfUse" component={TermsOfUse}  options={{title:"Terms & Condition",headerStyle:{borderBottomColor:'#e6e6e6',borderBottomWidth:wp('0.4%')}}}/>
        <Stack.Screen name="EditProfile" component={EditProfile}  options={{title:"Edit Profile",headerStyle:{borderBottomColor:'#e6e6e6',borderBottomWidth:wp('0.4%')}}}/>
        <Stack.Screen name="RecentActivity" component={RecentActivity}  options={{title:"Recent Activity",headerStyle:{borderBottomColor:'#e6e6e6',borderBottomWidth:wp('0.4%')}}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
const styles = StyleSheet.create({
  circleIcon:{
    width:50,
    height: 50,
    borderRadius: 25,
    borderWidth:2,
    borderColor: "white",
    alignSelf:'center',
    marginBottom:hp('2%'),
    backgroundColor:'#C1C0C9',
    alignItems:'center',
    justifyContent:'center',
  }
})