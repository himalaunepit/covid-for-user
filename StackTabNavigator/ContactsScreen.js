

import React,{useEffect} from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight ,Image} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
export default class ContactsScreen extends React.Component {
    constructor(props){
        super(props);
        this.state={
    
        }
    }
    
  render(){
    return (
      <View style={styles.container}>
        <View style={styles.headerStyle}>
                    <Text style={{marginLeft:wp('37%'),fontWeight:'bold',fontSize:wp('4%'),marginTop:hp('3%')}}>Profile</Text>
                    <MaterialCommunityIcons name="content-save-edit" style={{marginTop:hp('3%')}} size={wp('6%')} onPress={()=>this.props.navigation.navigate('EditProfile')} />
             </View>
             <View style={styles.coverStyle}>
             <View style={styles.header}>
                 <Image  source={require('../assets/pic.jpg')} style={{width:"100%",height:hp('23%'),position:'absolute'}}/>
              </View>
              <View style={{justifyContent:'flex-end',alignItems:'flex-end',marginRight:wp('2%')}}>
              <View style={{height:25,width:25,borderRadius:13,backgroundColor:'white',justifyContent:'center',alignItems:'center',marginTop:6}}>
              <MaterialCommunityIcons name="camera" color={'black'}/>
              </View>
              </View>
             
          <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
          <Image style={styles.avatar} source={require('../assets/coverpic.jpg')}/>
          <View style={{height:25,width:25,borderRadius:13,backgroundColor:'white',justifyContent:'center',alignItems:'center',marginLeft:115}}>
            <MaterialCommunityIcons name="camera" color={'black'}/>
          </View>
          </View> 

          <View style={styles.body}>
            <View style={{flexDirection:'row',justifyContent:'center'}}>
               <Text style={styles.name}>Welcome Himal</Text>
              <MaterialCommunityIcons name="hand-left" size={wp('5%')} style={{marginLeft:wp('1%'),marginTop:hp('0.7%')}}/>
            </View>
            <View style={styles.bodyContent}>
              <Text style={styles.info}>Himal Rawal </Text>
              <View style={{marginTop:hp('3%'),alignItems:'center',justifyContent:'center', marginLeft:wp('10%'),}}>
              <Text style={styles.information}>Tikapur,Kailali </Text>
              <Text style={styles.information}>Mobile Number: 9860552717 </Text>
              </View>
              
            </View>
        </View>
             </View>

       </View>
      );
}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerStyle:{
    height:hp('8%'),
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    width:wp('95%'),
    marginTop:hp('2%'),
    borderBottomColor:'#e6e6e6',
    borderBottomWidth:wp('0.4%')
  
  },
  coverStyle:{
    flex:2,
    margin:wp('3%'),
    width:wp("95%")
  },
  profileStyle:{
    flex:4,
  },
  header:{
    backgroundColor: "white",
    height:hp('20%'),
  },
  avatar: {
    width:110,
    height:110,
    borderRadius:55,
    borderWidth: 3,
    borderColor: "white",
    marginBottom:hp('3%'),
    alignSelf:'center',
    position: 'absolute',
  },
  body:{
    marginTop:hp('7%'),
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding:wp('3%'),
  },
  name:{
    fontSize:wp('6%'),
    color: "#262628",
    fontWeight: "bold"
  },
  info:{
    fontSize:wp('6%'),
    color: "#262628",
    marginLeft:wp('7%'),
    fontWeight: "bold"
  },
  information:{
    fontSize:wp('3%'),
    color: "#C1C0C9",
  },
})
  
  



